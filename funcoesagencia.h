#include <iostream>
#include <cstdlib>
#include <string>
#include <map>

using namespace std;

class agencia
{
  private: 
        string banco;
        string endereco;
        string cidade;
        string bairro;
        string estado;
        int cep;
        double telefone;

  public:
        agencia();
        agencia(string banco, string endereco, string cidade, string bairro, string estado, int cep, double telefone);
        ~agencia();
        string getbanco();      
        void setbanco(string banco);
        string getendereco();
        void setendereco(string endereco);
        string getcidade();
        void setcidade(string cidade);
        string getbairro();
        void setbairro(string bairro);
        string getestado();
        void setestado(string estado);
        int getcep();
        void setcep(int cep);
        double gettelefone();
        void settelefone(double telefone);
        void mostraagencia();
};    

