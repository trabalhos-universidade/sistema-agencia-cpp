#include "funcoesagencia.h"

agencia::~agencia()
{
}

agencia::agencia()
{
}

agencia::agencia(string banco, string endereco, string cidade, string bairro, string estado, int cep, double telefone)
{
    this->banco = banco;
    this->endereco = endereco;
    this->cidade = cidade;
    this->bairro = bairro;
    this->estado = estado;
    this->cep = cep;
    this->telefone = telefone;    
}    

string agencia::getbanco()
{
    return banco;
} 

void agencia::setbanco(string banco)
{
    this->banco = banco;
}        

string agencia::getendereco()
{
    return endereco;
}    

void agencia::setendereco(string endereco)
{
    this->endereco = endereco;
}
    
string agencia::getcidade()
{
    return cidade;
}    

void agencia::setcidade(string cidade)
{
    this->cidade = cidade;
}    

string agencia::getbairro()
{
    return bairro;
}    

void agencia::setbairro(string bairro)
{
    this->bairro = bairro;
}    

string agencia::getestado()
{
    return estado;
}    

void agencia::setestado(string estado)
{
    this->estado = estado;    
}   
 
int agencia::getcep()
{
    return cep;   
}  

void agencia::setcep(int cep)  
{
    this->cep = cep;
}    

double agencia::gettelefone()
{
    return telefone;
}    

void agencia::settelefone(double telefone)
{
    this->telefone = telefone;
}    

void agencia::mostraagencia()
{
    cout<<endl<<endl<<"Registro";
    cout<<endl<<"Nome: "<<banco;
    cout<<endl<<"Endereco: "<<endereco;
    cout<<endl<<"Cidade: "<<cidade;
    cout<<endl<<"Bairro: "<<bairro;
    cout<<endl<<"Estado: "<<estado;
    cout<<endl<<"Cep: "<<cep;
    cout<<endl<<"Telefone: "<<telefone;
}    
